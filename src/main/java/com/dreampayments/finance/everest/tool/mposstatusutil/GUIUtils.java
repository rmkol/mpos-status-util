package com.dreampayments.finance.everest.tool.mposstatusutil;

import javax.swing.*;
import java.awt.*;

public class GUIUtils {
    public static void setComponentSize(int width, int height, Component component) {
        Dimension size = new Dimension(width, height);
        component.setPreferredSize(size);
        component.setMinimumSize(size);
        component.setMaximumSize(size);
    }

    public static void runInNewThread(Runnable runnable) {
        new Thread(runnable).start();
    }

    public static void runInGuiThread(Runnable runnable) {
        SwingUtilities.invokeLater(runnable);
    }
}
