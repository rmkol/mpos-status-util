package com.dreampayments.finance.everest.tool.mposstatusutil;

import javax.swing.*;
import java.awt.*;
import java.io.PrintWriter;
import java.io.StringWriter;

public class Dialogs {
    public static void showMessageDialog(String message) {
        showMessageDialog(message, null);
    }

    public static void showMessageDialog(String message, Component parent) {
        JOptionPane.showMessageDialog(parent, message);
    }

    public static void showErrorDialog(String title, String message) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
    }

    public static void showErrorDialog(String message, Component parent) {
        JOptionPane.showMessageDialog(parent, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public static void showErrorDialog(Throwable throwable) {
        showErrorDialog(throwable, null);
    }

    public static void showErrorDialog(Throwable throwable, Component parent) {
        showErrorDialog(throwable, parent, "Error");
    }

    public static void showErrorDialog(Throwable throwable, Component parent, String title) {
        JScrollPane scrollPane = new JScrollPane();
        JTextArea textArea = new JTextArea(stackTraceAsString(throwable));
        textArea.setFont(new Font("Segoe UI", Font.PLAIN, 12));
        scrollPane.setViewportView(textArea);
        GUIUtils.setComponentSize(600, 500, scrollPane);
        JOptionPane.showMessageDialog(parent, scrollPane, title, JOptionPane.ERROR_MESSAGE);
    }

    private static String stackTraceAsString(Throwable throwable) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        throwable.printStackTrace(pw);
        return sw.toString();
    }
}
