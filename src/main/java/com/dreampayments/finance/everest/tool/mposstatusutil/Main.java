package com.dreampayments.finance.everest.tool.mposstatusutil;

import java.awt.*;

import static com.dreampayments.finance.everest.tool.mposstatusutil.Commons.print;
import static com.dreampayments.finance.everest.tool.mposstatusutil.Dialogs.showMessageDialog;
import static com.dreampayments.finance.everest.tool.mposstatusutil.GUIUtils.runInGuiThread;
import static java.lang.System.exit;

public class Main {
    public static void main(String[] args) {
        if (!SystemTray.isSupported()) {
            showMessageDialog("System tray feature isn't supported on your OS. App will exit.");
            exit(1);
        }

        if (args.length != 5) {
            print("wrong arguments provided");
            print("please provide following arguments:");
            print("1 - project name");
            print("2 - docker configurator container name");
            print("3 - docker fsdp container name");
            print("4 - mpos ear name");
            print("5 - mpos app name (URL part)");
            exit(1);
        }

        String projectName = args[0];
        String configuratorContainer = args[1];
        String fsdpContainer = args[2];
        String mposEarName = args[3];
        String mposAppName = args[4];

        print("starting app with following arguments:");
        print(" project name = " + projectName);
        print(" docker configurator container name = " + configuratorContainer);
        print(" docker fsdp container name = " + fsdpContainer);
        print(" mpos ear name = " + mposEarName);
        print(" mpos app name = " + mposAppName);

        runInGuiThread(() -> new App(
                projectName, configuratorContainer, fsdpContainer, mposEarName, mposAppName
        ).start());
    }
}
