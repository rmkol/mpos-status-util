package com.dreampayments.finance.everest.tool.mposstatusutil;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.dreampayments.finance.everest.tool.mposstatusutil.Commons.print;
import static com.dreampayments.finance.everest.tool.mposstatusutil.Dialogs.showErrorDialog;
import static com.dreampayments.finance.everest.tool.mposstatusutil.GUIUtils.runInGuiThread;
import static com.dreampayments.finance.everest.tool.mposstatusutil.GUIUtils.runInNewThread;

public class App {

    private Image iconGreen;
    private Image iconOrange;
    private Image iconRed;
    private Image iconPlain;
    private Image currentIcon;

    private String[] deployMPosCommand;
    private String startMPosWsCommand;
    private String stopMPosWsCommand;

    private String mPosHomeUrl;
    private String wsHomeUrl = "https://localhost:9043/ibm/console/secure/securelogon.do";
    private Menu wsMenu;

    private Menu mPosMenu;
    private MenuItem mPosDeployItem;
    private MenuItem mPosStartItem;
    private MenuItem mPosStopItem;

    private boolean wsUp = false;
    private boolean mPosUp = false;

    private boolean deployStarted = false;
    private boolean startStarted = false;
    private boolean stopStarted = false;

    private SystemTray tray;
    private TrayIcon trayIcon;

    private ScheduledThreadPoolExecutor executor;
    private ScheduledFuture animationTask;
    private boolean plainImgSet = false;

    private String projectName;
    private String fsdpContainer;

    public App(String projectName, String configuratorContainer, String fsdpContainer,
               String mposEarName, String mposAppName) {
        this.projectName = projectName;
        this.fsdpContainer = fsdpContainer;
        this.mPosHomeUrl = "http://localhost:8080/" + mposAppName;
        this.deployMPosCommand = new String[]{"docker", "exec", "-i", configuratorContainer, "fsdp", "deploy"};
        this.startMPosWsCommand = ("AdminControl.invoke(AdminControl.queryNames('cell=${project}-" +
                "fsdpNode01Cell,node=${project}-fsdpNode01,type=ApplicationManager,process=server1,*'), " +
                "'startApplication', '${appName}')")
                .replace("${project}", projectName)
                .replace("${appName}", mposEarName);
        this.stopMPosWsCommand = ("AdminControl.invoke(AdminControl.queryNames('cell=${project}-" +
                "fsdpNode01Cell,node=${project}-fsdpNode01,type=ApplicationManager,process=server1,*'), " +
                "'stopApplication', '${appName}')")
                .replace("${project}", projectName)
                .replace("${appName}", mposEarName);
    }

    public void start() {
        initializeTrayMenu();
        executor = new ScheduledThreadPoolExecutor(1);
        executor.scheduleAtFixedRate(this::refreshStatus, 0, 15, TimeUnit.SECONDS);
    }

    private void initializeTrayMenu() {
        tray = SystemTray.getSystemTray();

        if (isMac()) {
            iconGreen = readIconImage("icon-green-mac.png");
            iconOrange = readIconImage("icon-orange-mac.png");
            iconRed = readIconImage("icon-red-mac.png");
            iconPlain = readIconImage("icon-plain-mac.png");
        } else {
            iconGreen = readIconImage("icon-green.png");
            iconOrange = readIconImage("icon-orange.png");
            iconRed = readIconImage("icon-red.png");
            iconPlain = readIconImage("icon-plain.png");
        }

        updateTrayIcon(iconRed);

        PopupMenu popup = new PopupMenu();

        MenuItem projectNameItem = new MenuItem(projectName);
        projectNameItem.setEnabled(false);

        wsMenu = new Menu("WebSphere is [resolving...]");
        MenuItem wsHomePageItem = new MenuItem("home page");
        wsMenu.add(wsHomePageItem);

        mPosMenu = new Menu("mPOS is [resolving...]");
        MenuItem mPosHomePageItem = new MenuItem("home page");
        mPosDeployItem = new MenuItem("deploy");
        mPosStartItem = new MenuItem("start");
        mPosStopItem = new MenuItem("stop");
        mPosMenu.add(mPosHomePageItem);
        mPosMenu.add(mPosDeployItem);
        mPosMenu.add(mPosStartItem);
        mPosMenu.add(mPosStopItem);

        MenuItem refreshItem = new MenuItem("Refresh");
        MenuItem exitItem = new MenuItem("Exit");

        //Add components to pop-up menu
        popup.add(projectNameItem);
        popup.add(wsMenu);
        popup.add(mPosMenu);
        popup.addSeparator();
        popup.addSeparator();
        popup.add(refreshItem);
        popup.addSeparator();
        popup.add(exitItem);

        wsHomePageItem.addActionListener(e -> openUrlInBrowser(wsHomeUrl));

        mPosHomePageItem.addActionListener(e -> openUrlInBrowser(mPosHomeUrl));
        mPosDeployItem.addActionListener(e -> deployMpos());
        mPosStartItem.addActionListener(e -> startMPos());
        mPosStopItem.addActionListener(e -> stopMPos());

        refreshItem.addActionListener(e -> runInNewThread(this::refreshStatus));
        exitItem.addActionListener(e -> System.exit(0));

        trayIcon.setPopupMenu(popup);

        print("gui added");
    }

    private void deployMpos() {
        deployStarted = true;
        startIconAnimation();
        runInNewThread(() -> {
            try {
                refreshStatus();
                runCommand(deployMPosCommand);
            } finally {
                deployStarted = false;
                runInGuiThread(() -> {
                    stopIconAnimation();
                    refreshStatus();
                });
            }
        });
    }

    private void startMPos() {
        startStarted = true;
        startIconAnimation();
        runInNewThread(() -> {
            try {
                refreshStatus();
                runWsCommand(startMPosWsCommand);
            } finally {
                startStarted = false;
                runInGuiThread(() -> {
                    stopIconAnimation();
                    refreshStatus();
                });
            }
        });
    }

    private void stopMPos() {
        stopStarted = true;
        startIconAnimation();
        runInNewThread(() -> {
            try {
                refreshStatus();
                runWsCommand(stopMPosWsCommand);
            } finally {
                stopStarted = false;
                runInGuiThread(() -> {
                    stopIconAnimation();
                    refreshStatus();
                });
            }
        });
    }

    private void runWsCommand(String wsCommand) {
        String[] command = {
                "docker",
                "exec",
                "-i",
                fsdpContainer,
                "wsadmin.sh",
                "-conntype", "SOAP",
                "-host", "localhost",
                "-port", "8880",
                "-username", "admin",
                "-password", "password",
                "-lang", "jython",
                "-c", wsCommand
        };
        runCommand(command);
    }

    private void runCommand(String[] command) {
        try {
            Process process = Runtime.getRuntime().exec(command);
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                print(line);
            }
            process.waitFor();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
            showErrorDialog(e);
        }
    }

    @SuppressWarnings("ConstantConditions")
    private Image readIconImage(String imageName) {
        try {
            return ImageIO.read(getClass().getClassLoader().getResource(imageName));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void updateTrayIcon(Image icon) {
        try {
            if (trayIcon == null) {
                trayIcon = new TrayIcon(icon);
                tray.add(trayIcon);
                trayIcon.setImageAutoSize(true);
            } else {
                trayIcon.setImage(icon);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private synchronized void refreshStatus() {
        String status;

        if (isUrlAccessible(wsHomeUrl)) {
            status = "[UP]";
            wsUp = true;
        } else {
            wsUp = false;
            status = "[DOWN]";
        }
        wsMenu.setLabel("WebSphere is " + status);

        if (isUrlAccessible(mPosHomeUrl)) {
            status = "[UP]";
            mPosUp = true;
        } else {
            mPosUp = false;
            status = "[DOWN]";
        }
        mPosMenu.setLabel("mPOS is " + status);

        if (wsUp && mPosUp) {
            currentIcon = iconGreen;
            updateTrayIcon(iconGreen);
        } else if (wsUp || mPosUp) {
            currentIcon = iconOrange;
            updateTrayIcon(iconOrange);
        } else {
            currentIcon = iconRed;
            updateTrayIcon(iconRed);
        }

        runInGuiThread(() -> {
            if (wsUp) {
                if (deployStarted) {
                    mPosDeployItem.setEnabled(false);
                    mPosDeployItem.setLabel("deploying...");
                } else {
                    mPosDeployItem.setEnabled(true);
                    mPosDeployItem.setLabel("deploy");
                }
                if (mPosUp) {
                    mPosStartItem.setEnabled(false);
                    mPosStartItem.setLabel("start");
                    if (stopStarted) {
                        mPosStopItem.setEnabled(false);
                        mPosStopItem.setLabel("stopping...");
                    } else {
                        mPosStopItem.setEnabled(true);
                        mPosStopItem.setLabel("stop");
                    }
                } else {
                    if (startStarted) {
                        mPosStartItem.setEnabled(false);
                        mPosStartItem.setLabel("starting...");
                    } else {
                        mPosStartItem.setEnabled(true);
                        mPosStartItem.setLabel("start");
                    }
                    mPosStopItem.setEnabled(false);
                    mPosStopItem.setLabel("stop");
                }
            } else {
                mPosDeployItem.setEnabled(false);
                mPosStartItem.setEnabled(false);
                mPosStopItem.setEnabled(false);
            }
        });
    }

    private synchronized void startIconAnimation() {
        if (animationTask != null) {
            stopIconAnimation();
        }
        animationTask = executor.scheduleAtFixedRate(() -> {
            if (plainImgSet) {
                updateTrayIcon(currentIcon);
                plainImgSet = false;
            } else {
                updateTrayIcon(iconPlain);
                plainImgSet = true;
            }
        }, 0, 1500, TimeUnit.MILLISECONDS);
    }

    private synchronized void stopIconAnimation() {
        animationTask.cancel(false);
        animationTask = null;
    }

    private void openUrlInBrowser(String url) {
        try {
            Desktop.getDesktop().browse(new URI(url));
        } catch (Exception e) {
            e.printStackTrace();
            showErrorDialog(e, null, "Couldn't open page");
        }
    }

    private boolean isMac() {
        return System.getProperty("os.name").toLowerCase().contains("mac");
    }

    private boolean isUrlAccessible(String url) {
        try {
            URL url_ = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url_.openConnection();
            conn.setRequestMethod("GET");
            InputStream stream = conn.getInputStream();
            stream.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
